1.关于开发环境
    开发环境，最能体现工作产出的效率；
    
1.IDE用什么环境写代码（写代码的效率）
    
        编写代码的工具
        
    webstorm 非常强大的工具，可以集成很多东西，功能非常强大，收费
    sublime  轻量级打开比较快，非常好用，免费
            （搜常用插件--代码提示，括号括号之间封闭的一些提示，一键生成html代码结构）
    vscode   微软推出的比较轻量级的开发工具，免费
    atom     github自己开源的一个编辑器，免费
    插件！插件！插件！！！ 插件提高效率
    快捷键会用  
    
2.git（代码版本管理，多人协作开发）

    正式的项目都需要代码版本管理；
    大型项目需要多人协作开发；
    Git和Linux是一个作者；
    网络Git服务器如：github.com  coding.net;
    一般公司代码非开源，都有自己的Git服务器；
    Git的基本操作必须很熟练；
    
  git--常用命令
  
    git add . 
        （添加当前项目的所有文件--修改的新增的）
    git checkout xxx  
        撤销对工作区的修改（发现自己改错了想还原回去之前改的不要了）
    git commit -m"xxx" 
        把改的东西先提到本地，-m就是我要加个备注，改的什么东西
    git push origin master 
        提交到远程的服务器上了，别人就可以看到，就可以下载
    git pull origin master 
        把远程服务器上更新过的东西下载下来
    git branch  
        多人开发（查看当前的分支）
    git checkout -b xxx
        (新建一个分支)
    /git checkout xxx
        （切换到已有的分支）
    git merge xxx  
        把以前在那个分支改的东西全都merge到某一个分支上来,
        将两个或两个以上的开发历史合并在一起
    git status 看状态 看有哪些改动
    pwd 查看当前路径
    cd ../回上一个目录
    
    vi 打开aaa.txt :wq退出并保存
    
3.JS模块化

    1.不适用模块化的情况（场景使用，遇到的问题）
        util.js(基础函数库/、底层代码) 比如：getFormatDate函数
        a-util.js（针对业务又封装了一个基础库、业务层工具函数） aGetFormatDate函数（要依赖getFormatDate来用） 使用getFormatDate
        a.js（业务代码需要使用a-util.js、一层一层的引用关系） aGetFormatDate
        
        eg:
        //util.js ----文件
        function getFormatDate(date,type){
            //type ===1 返回 2017-06-15
            //type ===2 返回 2017年6月15日 格式
            // ___
        }
        
        //a-util.js ----文件
        function aGetFormatDate(date){
            //要求返回 2017年6月15日 格式
            return getFormatDate(date,2);
        }
        
        //a.js  ----文件
        var dt = new Date();
        console.log(aGetFormatDate(dt));
        
        *使用：（强依赖的引用关系，顺序不能乱了，会报错）
        <script src="util.js"></script>
        <script src="a-util.js"></script>
        <script src="a.js"></script>
        
        <!--1.这些代码中的函数必须是全局变量，才能暴露给使用方，全局变量污染-->
        <!--2.a.js 知道要引用a-util.js,但是他知道还需要依赖于util.js吗？-->
    2.使用模块化（好处）
        //util.js （只往外输出）
        export{
            getFormatDate:function(date,type){
                // type === 1返回 2017-06-15
                // type === 2 返回 2017年6月15日 格式
            }
        };
        
        //a-util.js  
        //require 引用（接到输出的函数，自己又输出一个；用什么就引用什么，引用完之后还能传递出自己的一个函数）
        var getFormatDate = require('util.js');
        export{
            aGetFormatDate:function(date){
                //要求返回 2017年6月15日 格式
                return getFormatDate(date);
            };
        };
        
        //a.js（把你输出的接收过来执行）
        var aGetFormatDate = require("a-util.js");
        var dt = new Date();
        console.log(aGetFormatDate(dt));
        //直接'<script src="a.js"></script>',其他的根据依赖关系自动引用；
        //那两个函数，没必要成为全局变量，不会带来污染和覆盖
        
    3.AMD 异步模块定义
        *require.js    requirejs.org/
        1.全局define函数
        2.全局require函数
        3.依赖JS会自动、异步加载
        
        
        //只有define的东西才能require
        //先去定义一个东西，然后就能从require中拿到这个东西
        //util.js
        define(function(){
            return{
                getFormatDate:function(date,type){
                    if(type ===1){
                        return '2017-06-15';
                    }
                    if(type ===2){
                        return"2017年6月15日"
                    }
                }
            }
        });
        
        //a-util.js
        define(['./util.js'],function(util){
            return{
                aGetFormatDate:function(date){
                    return util.getFormatDate(date,2);
                }
            };
        });
        
        //a.js 要依赖于a-util.js
        define(['./a-util.js'],function(aUtil){
            return{
                printDate:function(date){
                    console.log(aUtil.aGetFormatDate(date));
                }
            };
        });
        
        //main.js 要依赖a.js（可以依赖于多个）
        require(['./a.js'],function(a){
            var date = new Date();
            a.printDate(date);


    *使用require.js
    <body>
        <p>AMD test</p>
        
        //date-main="./main.js" 程序的入口
        <script src="/require.min.js" data-main="./main.js"></script>
    </body>
    4.CommonJS
    
    
4.打包工具
5.上线回滚的流程
（我虽然没用过，但是通过XXX知道，它应该是个什么样的流程）